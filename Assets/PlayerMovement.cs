﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

    public class PlayerMovement : MonoBehaviour
    {
        public CharacterController controller;
        public float speed;

        private Vector3 velocity;
        public Transform groundCheck;
        public float groundDistance = 0.4f;

        public float jumpHeight = 4f;
        public LayerMask groundMask;
        public float gravity = -45f;
        public bool canPlayerMove;

        private float stamina = 100.0f;

        private bool isGrounded;

        public float maxSprint = 100.0f;
        public float currentSprint;
        private bool canSprint;
        private bool canKeepSprinting;
        [SerializeField]
        private bool isSprinting;

        private void Start()
        {
            //set variables to values
            currentSprint = maxSprint;
        }


        void Update()
        {
            //set bool is grounded to Sphere of Object
            isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);


            if (isGrounded && velocity.y < 0)
            {
                velocity.y = -2f;
            }

            //get axis values
            float x = Input.GetAxis("Horizontal");
            float z = Input.GetAxis("Vertical");

            //set Vector3 to new movement transform
            Vector3 move = transform.right * x + transform.forward * z;
            controller.Move(move * (speed * Time.deltaTime));

            //is key pressed then jump (jumpHeight and gravity)
            if (Input.GetButton("Jump") && isGrounded)
            {
                velocity.y = Mathf.Sqrt(jumpHeight * -2 * gravity);
            }

            //set slider value to currentSprint


            // if bool false then set canSprint false
            if (!canKeepSprinting)
            {
                canSprint = false;

            }

            //if currentSprint >=20f set bool values true
            if (currentSprint >= 20f)
            {
                canSprint = true;
                canKeepSprinting = true;
            }

            SprintChange();

            //if key pressed then set speed value to new value
            if (Input.GetKeyDown(KeyCode.LeftShift) && canSprint)
            {
                speed = 20f;
                isSprinting = true;
                Debug.Log("KEYDOWN");
            }

            //if value <= 0 set value to 0 and speed to new value, set bool variables to false to not be able to sprint
            if (currentSprint <= 0f)
            {
                currentSprint = 0f;
                speed = 12f;
                canSprint = false;
                isSprinting = false;
            }

            //if key pressed and value above 20, then bool value false
            if (Input.GetKeyDown(KeyCode.LeftShift) && currentSprint <= 20f && canSprint)
            {
                canKeepSprinting = false;

            }

            //if key up set speed to value and bool variable to false
            if (Input.GetKeyUp(KeyCode.LeftShift))
            {
                speed = 12f;
                isSprinting = false;
            }

            velocity.y += gravity * Time.deltaTime;
            controller.Move(velocity * Time.deltaTime);
        }


        void SprintChange()
        {
            //lose sprint if key is pressed and player able to sprint
            if (Input.GetKey(KeyCode.LeftShift) && canSprint && isSprinting)
            {
                currentSprint -= 20f * Time.deltaTime;
                Debug.Log("SPRINTING");
            }

            //gain sprint if player is not sprinting
            else
            {
                currentSprint += 4 * Time.deltaTime;
                Debug.Log("STOPPED SPRINTING");

                //set sprint value to 100 if above 100 to cap it
                if (currentSprint >= 100f)
                {
                    currentSprint = 100f;
                }
            }
        }
    }
