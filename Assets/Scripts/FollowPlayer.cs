﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class FollowPlayer : MonoBehaviour
{
    public Transform player;

    public Vector3 offsetPlayer;
 
    
    void Update()
    {
        transform.position = player.position + offsetPlayer;
    }
}
